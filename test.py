import time
import re
import requests
from bs4 import BeautifulSoup
import xlwt
import json

findname = re.compile(r'<a class="hotel-name".*>(.*?)</a>')
findgrade = re.compile(r'<span class="num">(3|4|("4))\.(.*?)</span><span class="desc">')
findtotal = re.compile(r'<span class="total">共(.*)条评论</span>')
findprice = re.compile(r'<span class="y rmb">¥</span>(.*)<span class="qi">起</span>')
finddetail = re.compile(r'<a class="btn hotel-card-detail-btn" (.*?)" rel="noopener noreferrer" target="_blank" title=.*>查看详情</a>')

def askurl(city, i, url):  #获取网页内容（post）
    request_payload = {
        "b": "{bizVersion: \"17\", cityUrl:" + city + ", cityName: \"\", fromDate: \"2021-03-23\", toDate: \"2021-03-24\", q: \"\",…}",
        "bizVersion": "17",
        "channelId": 1,
        "cityName": "",
        "cityType": 1,
        "cityUrl": city,
        "comprehensiveFilter": [],
        "fromDate": "2021-03-23",
        "fromForLog": 1,
        "hourlyRoom": "false",
        "level": "",
        "locationAreaFilter": [],
        "maxPrice": -1,
        "minPrice": 0,
        "num": 20,
        "q": "",
        "qFrom": 3,
        "searchType": 0,
        "sort": 0,
        "start": int(i*20),
        "toDate": "2021-03-24",
        "userId": "",
        "userName": "dmnabfc0805",
        "uuid": "",
        "vtoken": "pclist-v1-89412dc54851e3f68e034ceba77df341"
    }
    head = {
        "user-agent":"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36"
    }
    response = requests.post(url, headers=head, data=json.dumps(request_payload))
    #headers里表示这里的数据获取是post方法，所以使用requests.post函数
    return response.text


def getlist(city, url):
    hotellist = []

    for z in range(0, 3):  # 爬取页数设置
        page = askurl(city, z, url) #爬取第z页
        soup = BeautifulSoup(page, 'html.parser')  #是一个树形结构了
        lsts = soup.find_all('div', class_="inner clearfix" )

        ##表空判断
        if not lsts:
            print("No targets found")
            print("连接到网页失败")
            exit(0)

        print("链接网页成功，开始爬取数据")
        number = 1
        #非空情况下读取
        for item in lsts:
            hotel = []             #每个hotel存放一个酒店的信息（列表形式）
            item = str(item)

            # 酒店名称
            hotel_name = re.findall(findname, item)[0]
            hotel.append(hotel_name)

            # 酒店评分
            hotel_grade = re.findall(findgrade, item)
            temp = list(hotel_grade)
            if temp:
                hotel.append(temp[0][0])
                hotel.append(temp[0][2])
            else:
                hotel.append(0)
                hotel.append(0)

            # 酒店总评分数
            hotel_total = re.findall(findtotal, item)[0]
            hotel.append(hotel_total)

            # 酒店起步价
            hotel_price = re.findall(findprice, item)
            if len(hotel_price):
                hotel_price = hotel_price[0]
            else:
                hotel_price = 0
            hotel.append(hotel_price)

            # 详情链接
            hotel_info = re.findall(finddetail, item)[0]
            hotel.append(hotel_info)

            # 写入hotellist
            hotellist.append(hotel)

            print("-----正在爬取第%d条酒店信息-----"%number)
            number += 1
            time.sleep(1.5)
        time.sleep(7.5)
        print("第%d页爬取完成"%(z+1))
    return hotellist

def listToExcel(city, list):
    col = ['酒店名称', '酒店评分整数', '酒店评分小数', '酒店评价总数', '起步价', '详情网址']
    hotelbook = xlwt.Workbook(encoding = "utf-8", style_compression = 0)
    hotelsheet = hotelbook.add_sheet("sheet1", cell_overwrite_ok = True)
    for i in range(len(col)):
        hotelsheet.write(0, i, col[i])

    for i in range(0,len(list)):
        print("-----正在写入第%d条酒店信息-----"%(i+1))
        item = list[i]
        for j in range(len(col)):
            hotelsheet.write(i+1, j, item[j])

    hotelbook.save(city + "hotel.xls")

def main():
    city = "beijing_city"
    #基本上写入城市拼音即可，但是北京要写成beijing_city
    baseurl = "https://hotel.qunar.com/cn/" + city + "/?fromDate=2021-03-23&url=beijing_city&toDate=2021-03-24"
    hotellist = getlist(city, baseurl)
    listToExcel(city, hotellist)
    #askurl(baseurl)

if __name__ == '__main__':
    main()

