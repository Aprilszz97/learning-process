import json
import requests
import pandas as pd
cookies = {
    '$Cookie: JSESSIONID': '586976FF5E3D7B9381A5E073429E7253',
    'XSRF-CCKTOKEN': '0613323b96501ba49472fce33531aba2',
    'CHSICC01': '\\u0021RD0y1g196YlDY/zzYxYLahOzddj6Y38FK4nxl\
    +N0BaReOMegnJQi0LSR6a6Ud3meEAxNI+NWDrDp',
    'CHSICC_CLIENTFLAGCHSI': '60249618f504ea500b03b52cc74a9807',
    'JSESSIONID': 'F1C0420489C913E29F21F779108D34AD',
}

headers = {
    'Connection': 'keep-alive',
    'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
    'Accept': 'application/json, text/plain, */*',
    'X-Requested-With': 'XMLHttpRequest',
    'sec-ch-ua-mobile': '?0',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36\
     (KHTML, like Gecko) Chrome/91.0.4472.164 Safari/537.36',
    'Sec-Fetch-Site': 'same-origin',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Dest': 'empty',
    'Referer': 'https://gaokao.chsi.com.cn/zyk/zybk/zyjd/listPage',
    'Accept-Language': 'zh-CN,zh;q=0.9',
}
id_list1 = []
for i in range(0, 100, 9):
    params = (
        ('timestamp', '1626665528085'),
        ('start', i),
        ('ml', ''),
        ('cc', '\u672C\u79D1\uFF08\u666E\u901A\u6559\u80B2\uFF09'),
    )
    response = requests.get('https://gaokao.chsi.com.cn/zyk/zybk/zyjd/listInfo',
                            headers=headers, params=params,cookies=cookies)

    resp = response.json()
    id_list = [item.get('zyjdId') for item in resp.get('msg', {}).get('list')]
    id_list1 = id_list1+id_list

# print(id_list1)
base_url = 'https://gaokao.chsi.com.cn/zyk/zybk/zyjd/view/'
urllist = []
for j in range(0, len(id_list1)):
    urllist.append(base_url + id_list1[j])
# print('获取链接列表：', urllist)
# print(requests.get(urllist[0]).json())

df_info_all = pd.DataFrame()
for t in range(0, len(urllist)):
    response = requests.get(urllist[t])
    msg_dic = json.loads(response.text).get('msg')
    msg_dic.update({'f_title': msg_dic.pop("title")})
    msg_dic.update({'专业名称': msg_dic.pop("zymc")})
    msg_dic.update({'作者': msg_dic.pop("author")})
    msg_dic.update({'专业简介': msg_dic.pop("gy")})
    df_info_temp2 = pd.DataFrame()

    msg_dic['专业解析'] = msg_dic['mlList'][0]['content']
    msg_dic['专业与就业'] = msg_dic['mlList'][1]['content']
    msg_dic['报考指南'] = msg_dic['mlList'][2]['content']
    length = len(msg_dic['mlList'][2]['zytjList'])

    for d in range(0, length):
        del_dic = msg_dic['mlList'][2]['zytjList'][d]
        del del_dic['cc']
        del del_dic['specId']
        del_dic.update({'院校名称': del_dic.pop("yxmc")})
        del_dic.update({'投票计数': del_dic.pop("count")})
        del_dic.update({'得分': del_dic.pop("rank")})
        df_info_temp = pd.DataFrame.from_dict(del_dic, orient='index').T
        df_info_temp2 = pd.concat([df_info_temp2, df_info_temp], axis=0)
    del msg_dic['mlList']
    del msg_dic['origin']
    df_info_temp1 = pd.DataFrame.from_dict(msg_dic, orient='index').T
    df_info_all = pd.concat([df_info_all, df_info_temp1, df_info_temp2], axis=0)

df_info_all.to_excel('专业解读列表.xlsx')
